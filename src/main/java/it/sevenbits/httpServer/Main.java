package it.sevenbits.httpServer;

import it.sevenbits.httpServer.Server.HttpServer;

/**
 * Main class
 */
final class Main {
    /**
     * Input point to application
     *
     * @param args - console arguments
     */
    public static void main(final String[] args) {
        final HttpServer httpServer = new HttpServer();

        final int serverPort = 8080;

        httpServer.start(serverPort);
    }

    private Main() {
    }
}
