package it.sevenbits.httpServer.Server;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class HttpServer {
    private ServerSocket serverSocket;

    /**
     * Start server
     *
     * @param port - server port
     */
    public void start(final int port) {
        try {
            serverSocket = new ServerSocket(port);
            while (true) {
                new EchoClientHandler(serverSocket.accept()).start();
            }
        } catch (Exception exception) {
            exception.printStackTrace();
            stop();
        }
    }

    /**
     * Stop server
     */
    public void stop() {
        try {
            serverSocket.close();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private static class EchoClientHandler extends Thread {
        private final Socket clientSocket;

        /**
         * Constructor
         *
         * @param socket - socket
         */
        EchoClientHandler(final Socket socket) {
            this.clientSocket = socket;
        }

        public void run() {
            try (
                    PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                    BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()))
            ) {
                final StringBuilder response = new StringBuilder();

                final String startLine = in.readLine();

                if (startLine != null) {
                    final String[] components = startLine.split(" ");

                    response.append(components[2]).append(" 200 OK\n")
                            .append("Server: HttpServer\n")
                            .append("Content-type: text/plain\n");

                    String inputLine;

                    while (!((inputLine = in.readLine()).equals(""))) {
                        if (inputLine.startsWith("Connection:")) {
                            response.append(inputLine).append("\n");
                        }
                    }
                    response.append("\nHello, ")
                            .append(components[1].substring(components[1].indexOf('=') + 1))
                            .append("!");
                    out.println(response);
                }

                clientSocket.close();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }
}
